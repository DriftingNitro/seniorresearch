# My project's README

The objective of this project is to test the theory that Virtual Reality offers a superior means of learning and memorization that rivals conventional methods, and provides new context for how we will teach kids in the near future.

The project will be written in C# using Unity engine and Valve VR Devkit.